<?php

namespace App\Http\Requests\Chart;

use App\Models\ChartForm;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetChartRequest
 * @package App\Http\Requests\Chart
 */
class GetChartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            ChartForm::COLUMN_EMAIL => [
                'required',
                'email'
            ],
            ChartForm::COLUMN_START_DATE => [
                'required',
                'date',
                'before_or_equal:today',
                'before_or_equal:' . ChartForm::COLUMN_END_DATE,
            ],
            ChartForm::COLUMN_END_DATE => [
                'required',
                'date',
                'before_or_equal:today',
                'after_or_equal:' . ChartForm::COLUMN_START_DATE
            ],
            ChartForm::COLUMN_SYMBOL => [
                'required',
                'string',
                'max:4'
            ]
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [];
    }
}
