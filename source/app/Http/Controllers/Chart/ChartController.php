<?php

namespace App\Http\Controllers\Chart;

use Exception;
use App\Models\ChartForm;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\Chart\GetChartRequest;
use App\Components\Charts\Services\ChartService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Components\Charts\Clients\RapidApiClient;
use App\Components\Charts\Formatter\FormatterFactory;
use App\Components\Charts\Clients\Factories\ClientFactory;

/**
 * Class ChartController
 * @package App\Http\Controllers\Chart
 */
class ChartController extends Controller
{
    /**
     * @param GetChartRequest $request
     * @param ChartForm $chartForm
     * @param ChartService $service
     * @param ClientFactory $clientFactory
     * @param FormatterFactory $formatterFactory
     * @return Application|ResponseFactory|Response
     * @throws Exception
     */
    public function charts(
        GetChartRequest $request,
        ChartForm $chartForm,
        ChartService $service,
        ClientFactory $clientFactory,
        FormatterFactory $formatterFactory
    ) {
        $chartForm->fill($request->all());
        $client = $clientFactory->build(RapidApiClient::TYPE); // will get from request in future if needed
        $formatter = $formatterFactory->build(RapidApiClient::TYPE);
        $charts = $service->getCharts($chartForm, $client, $formatter);

        return response(JsonResource::collection($charts));
    }
}
