<?php

namespace App\Components\Charts\Formatter;

use Carbon\Carbon;
use JsonException;

/**
 * Class RapidApiCharsetFormatter
 * @package App\Components\Charts\Formatter
 */
class RapidApiCharsetFormatter implements ResultsFormatterContract
{
    /**
     * @param string $response
     * @param $startDate
     * @param $endDate
     * @return array
     * @throws JsonException
     */
    public function format(string $response, $startDate, $endDate): array
    {
        $startDate = Carbon::parse($startDate);
        $endDate = Carbon::parse($endDate);
        $responseData = json_decode($response,true, 512, JSON_THROW_ON_ERROR);

        $result = [];
        if (
            $responseData['chart']
            && $responseData['chart']['result'][0]
            && $responseData['chart']['result'][0]['indicators']
            && $responseData['chart']['result'][0]['indicators']['quote']
        ) {
            $timestamps = $responseData['chart']['result'][0]['timestamp'];
            $quote = $responseData['chart']['result'][0]['indicators']['quote'][0];
            $opens = $quote['open'];
            $closes = $quote['close'];
            $highs = $quote['high'];
            $lows = $quote['low'];
            $volumes = $quote['volume'];

            foreach ($timestamps as $timestamp) {
                $date = Carbon::createFromTimestamp($timestamp);
                foreach ($opens as $open) {
                    foreach ($closes as $close) {
                        foreach ($highs as $high) {
                            foreach ($lows as $low) {
                                foreach ($volumes as $volume) {
                                    if ($date->between($startDate, $endDate)) {
                                        $result[] = [
                                            'date' => $date->toDateString(),
                                            'open' => $open,
                                            'close' => $close,
                                            'high' => $high,
                                            'low' => $low,
                                            'volume' => $volume
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}
