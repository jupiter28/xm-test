<?php

namespace App\Components\Charts\Formatter;

/**
 * Interface ResultsFormatterContract
 * @package App\Components\Charts\Formatter
 */
interface ResultsFormatterContract
{
    /**
     * @param string $response
     * @param $startDate
     * @param $endDate
     * @return array
     */
    public function format(string $response, $startDate, $endDate): array;
}
