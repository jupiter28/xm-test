<?php

namespace App\Components\Charts\Formatter;

use App\Components\Charts\Clients\RapidApiClient;
use Exception;

/**
 * Class FormatterFactory
 * @package App\Components\Charts\Formatter
 */
class FormatterFactory
{
    /**
     * @param string $type
     * @return ResultsFormatterContract
     * @throws Exception
     */
    public function build(string $type): ResultsFormatterContract
    {
        switch ($type) {
            case RapidApiClient::TYPE:
                return resolve(RapidApiCharsetFormatter::class);
            default:
                throw new Exception("Formatter type $type is not supported");
        }
    }
}
