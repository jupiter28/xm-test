<?php

namespace App\Components\Charts\Services;

use App\Components\Charts\Clients\ChartClientContract;
use App\Components\Charts\Formatter\ResultsFormatterContract;
use App\Models\ChartForm;

/**
 * Class ChartService
 * @package App\Components\Charts\Services
 */
class ChartService
{
    /**
     * @param ChartForm $chartForm
     * @param ChartClientContract $client
     * @param ResultsFormatterContract $formatter
     * @return array
     */
    public function getCharts(
        ChartForm $chartForm,
        ChartClientContract $client,
        ResultsFormatterContract $formatter
    ): array {
        $params = $client->prepareQueryParams($chartForm);
        $response = $client->getCharts($params);
        return $formatter->format($response->getBody()->getContents(), $chartForm->start_date, $chartForm->end_date);
    }
}
