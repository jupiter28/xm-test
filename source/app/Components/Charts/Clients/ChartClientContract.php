<?php

namespace App\Components\Charts\Clients;

use App\Models\ChartForm;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface ChartClientContract
 * @package App\Components\Charts\Clients
 */
interface ChartClientContract
{
    /**
     * @param array $options
     * @return ResponseInterface
     */
    public function getCharts(array $options = []): ResponseInterface;

    /**
     * @param ChartForm $chartForm
     * @return array
     */
    public function prepareQueryParams(ChartForm $chartForm): array;
}
