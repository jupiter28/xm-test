<?php

namespace App\Components\Charts\Clients\Factories;

use App\Components\Charts\Clients\ChartClientContract;
use App\Components\Charts\Clients\RapidApiClient;
use Exception;

/**
 * Class ClientFactory
 * @package App\Components\Charts\Clients\Factories
 */
class ClientFactory
{
    /**
     * @param string $type
     * @return ChartClientContract
     * @throws Exception
     */
    public function build(string $type): ChartClientContract
    {
        switch ($type) {
            case RapidApiClient::TYPE:
                return resolve(RapidApiClient::class);
            default:
                throw new Exception("Client type $type is not supported");
        }
    }
}
