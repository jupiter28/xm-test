<?php

namespace App\Components\Charts\Clients;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;

/**
 * Class AbstractChartClient
 * @package App\Components\Charts\Clients
 */
abstract class AbstractChartClient extends Client implements ChartClientContract
{
    public const BASE_URI = 'base_uri';

    public $host;
    protected $defaultOptions = [];

    /**
     * AbstractChartClient constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->setDefaultOptions();
        parent::__construct(array_replace_recursive($this->defaultOptions, $config));
    }

    /**
     * @return $this
     */
    protected function setDefaultOptions(): self
    {
        Arr::set($this->defaultOptions, self::BASE_URI, $this->getBaseUri());
        return $this;
    }

    /**
     * @return string
     */
    protected function getBaseUri(): string
    {
        return $this->host;
    }
}
