<?php

namespace App\Components\Charts\Clients;

use App\Models\ChartForm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Psr\Http\Message\ResponseInterface;

class RapidApiClient extends AbstractChartClient implements ChartClientContract
{
    public const TYPE = 'rapid';
    public const PARAM_SYMBOL = 'symbol';
    public const PARAM_INTERVAL = 'interval';
    public const PARAM_RANGE = 'range';

    public const ONE_YEAR = 365;
    public const ONE_MONTH = 30;

    public const HOST = 'https://apidojo-yahoo-finance-v1.p.rapidapi.com';
    public const X_RAPIDAPI_HOST = 'apidojo-yahoo-finance-v1.p.rapidapi.com';

    public const HEADER_RAPIDAPI_HOST = 'x-rapidapi-host';
    public const HEADER_RAPIDAPI_KEY = 'x-rapidapi-key';

    public const ROUTE_GET_CHARTS = '/market/get-charts';

    public const INTERVAL = '1d';

    public function __construct(array $config = [])
    {
        $this->host = self::HOST;
        parent::__construct($config);
    }

    /**
     * @param array $options
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCharts(array $options = []): ResponseInterface
    {
        return $this->request(
            Request::METHOD_GET,
            self::ROUTE_GET_CHARTS,
            array_replace_recursive($this->getRequestDefaultOptions(), $options)
        );
    }

    /**
     * @return array
     */
    protected function getRequestDefaultOptions(): array
    {
        return [
            'headers' => $this->getDefaultHeaders(),
        ];
    }

    /**
     * @return array
     */
    protected function getDefaultHeaders(): array
    {
        return [
            self::HEADER_RAPIDAPI_HOST => self::X_RAPIDAPI_HOST,
            self::HEADER_RAPIDAPI_KEY => config('app.rapidapi_key')
        ];
    }

    /**
     * @param ChartForm $chartForm
     * @return array
     */
    public function prepareQueryParams(ChartForm $chartForm): array
    {
        $range = $this->getRange(Carbon::parse($chartForm->start_date));

        return [
            'query' => [
                self::PARAM_SYMBOL => $chartForm->{ChartForm::COLUMN_SYMBOL},
                self::PARAM_INTERVAL => self::INTERVAL,
                self::PARAM_RANGE => $range
            ]
        ];
    }

    /**
     * @param Carbon $startDate
     * @return string
     */
    private function getRange(Carbon $startDate): string
    {
        $difference = $startDate->diffInDays(Carbon::today());

        if ($difference > 5 * self::ONE_YEAR) {
            $range = 'max';
        } elseif ($difference > self::ONE_YEAR) {
            $range = '5y';
        } elseif ($difference > 6 * self::ONE_MONTH) {
            $range = '1y';
        } elseif ($difference > 3 * self::ONE_MONTH) {
            $range = '6mo';
        } elseif ($difference > 5) {
            $range = '3mo';
        } elseif ($difference > 1) {
            $range = '5d';
        } else {
            $range = '1d';
        }
        return $range;
    }
}
