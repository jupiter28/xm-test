<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Chart
 * @package App\Models
 */
class Chart extends Model
{
    public const COLUMN_DATE = 'date';
    public const COLUMN_OPEN = 'open';
    public const COLUMN_HIGH = 'high';
    public const COLUMN_LOW = 'low';
    public const COLUMN_CLOSE = 'close';
    public const COLUMN_VOLUME = 'volume';
}
