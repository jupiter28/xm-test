<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChartForm
 * @package App\Models
 *
 * @property string $start_date
 * @property string $end_date
 * @property string $email
 * @property string $symbol
 */
class ChartForm extends Model
{
    public const COLUMN_SYMBOL = 'symbol';
    public const COLUMN_EMAIL = 'email';
    public const COLUMN_START_DATE = 'start_date';
    public const COLUMN_END_DATE = 'end_date';

    public $fillable = [
        self::COLUMN_SYMBOL,
        self::COLUMN_EMAIL,
        self::COLUMN_START_DATE,
        self::COLUMN_END_DATE
    ];

    public $guarded = [];
}
